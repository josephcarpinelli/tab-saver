#! /usr/bin/env python3

"""Saves Firefox browser tabs using keyboard shortcuts and PyAutoGUI.
Using the same method, it also can open the tabs from the created
file."""

import sys
import time
import logging
import traceback
import csv
from pathlib import Path

import pyperclip
import pyautogui

from modules import helpers


# # # Globals / Logger # # #

# # Path # #

Data = Path("data")
Logs = Data / "logs"

# # Logger # #

try:
    LogFile = Logs / "tabs.log"
    LogFmt = "%(asctime)s - %(levelname)s - %(message)s"
    logger = logging.getLogger(__name__)  # Get logger with module name
    fileHndl = logging.FileHandler(LogFile)
    # Get file handler  # mode='a' OR mode='w'
    streamHndl = logging.StreamHandler()  # Get stream handler
    formatter = logging.Formatter(LogFmt)  # Get formatter

    # Handle message levels
    logger.setLevel(logging.DEBUG)
    fileHndl.setLevel(logging.DEBUG)
    streamHndl.setLevel(logging.INFO)

    # Set formatter
    fileHndl.setFormatter(formatter)
    streamHndl.setFormatter(formatter)

    # Add file handlers
    logger.addHandler(fileHndl)
    logger.addHandler(streamHndl)

except RuntimeWarning:
    # Attempt to log setup failure
    print(traceback.format_exc())
    print("An exception occured... Logging disabled.")
    # Notify setup failure outside logging
    logging.disable()  # Disable logging after failed setup


def tab_saver(Directory: (Path, str)) -> None:
    """Saves several tabs open in Firefox browser and stores them to a CSV."""
    Timeout = 4

    tabs_path = Path(Directory).expanduser().absolute()
    tab_urls = []

    # # Request Information form and Alert User # #

    text = "Please enter the number of tabs to save: "
    tabcount = int(pyautogui.prompt(text))

    text = f"Click the first tab now, or within {Timeout} second(s) of pressing 'OK'."
    pyautogui.alert(text)
    helpers.countdown(Timeout)

    for index in range(tabcount):
        # Focus Searchbar (Ctrl + k)
        pyautogui.hotkey("ctrl", 'k')

        # Escape to current URL (Esc * 2)
        pyautogui.press("esc", 2)

        pyautogui.hotkey("ctrl", 'c')  # Copy URL
        pyautogui.hotkey("ctrl", "pgdn")  # Switch tab

        # Add URL to list for storage
        tab_urls.append(pyperclip.paste())

    # Save to file
    if True:  # Always save to JSON
        helpers.to_json(tab_urls, tabs_path.with_suffix(".json"))
    else:
        helpers.to_csv(tab_urls, tabs_path.with_suffix(".csv"))

    return None


def tab_opener(Directory: (Path, str)) -> None:
    """Opens new tabs after copying a list of links separated by a newline."""
    Timeout = 2
    tablinks = []

    tabs_path = Path(Directory).expanduser().absolute()

    # [tablinks] = helpers.load_csv(tabs_path)
    # [varname] to remove outer list
    tablinks = helpers.load_json(tabs_path)

    """ # Use Copy-Paste List i.e. from an Excel file
        print("Please copy the tabs with a newline between each tab...")
        input("Press enter when done: ")

        tablinks = [tablink for tablink in pyperclip.paste().split('\n')]
    """

    pyautogui.run("k'winleft' s0.54 w'Firefox' s0.2 k'enter' s0.5")  # Open Firefox
    helpers.countdown(Timeout)  # , "Press enter once the browser is under this window:")

    logger.info(f"\nOpening {len(tablinks)} tabs...")

    count = 1  # to avoid opening a new tab at the end
    pyautogui.hotkey("ctrl", 'k')  # Enter search
    pyautogui.hotkey("ctrl", 'a')  # Highligh '?' to replace for URLs
    for tablink in tablinks:
        pyperclip.copy(tablink)
        pyautogui.hotkey("ctrl", 'v')
        pyautogui.press("enter")

        if count == len(tablinks):  # last iteration?
            break

        pyautogui.hotkey("ctrl", 't')
        count += 1

    return None
