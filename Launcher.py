#! /usr/bin/env python3

"""A Tk GUI that launches Tab-Saver scripts."""

import logging
import traceback

from pathlib import Path
import tkinter as tk
from tkinter import filedialog

from modules import tabs


"""Issues
Remove dotfiles from find and save files
Also, path changes to project path during second file search.
"""

"""
from tkinter.filedialog import askopenfilename
add function from gen_harvest_csv:

def get_filename(Prompt: str) -> str:
    "#"#"Opens a Tk filedialog window for a user to select a file, that
    file's name is returned as a string."#"#"
    root = Tk()  # get a main window so we can destroy it later
    root.withdraw()  # hide main window so only the filedialog is visible

    # Workaround to hide hidden files
    # Undocumented usage, hence the outer try/catch block and bare
    # exception
    try:
        # Call a dummy dialog using a non-existant option
        # This will not give a file dialog, but will throw a TclError
        # This will create the parent namespace necessary to call
        # set commands below
        try:
            root.tk.call("tk_getOpenFile", "-foobarbaz")
        except Exception:  # _tkinter.TclError
            pass
        # Set inner/magic variables
        root.tk.call("set", "::tk::dialog::file::showHiddenBtn", '1')
        root.tk.call("set", "::tk::dialog::file::showHiddenVar", '0')
    except Exception:
        logger.warn(traceback.format_exc())
        logger.warn("An error occurred, hidden files will now be shown...")

    filename = askopenfilename(title=Prompt, filetypes=Filetypes)
    # get filename

    root.destroy()  # destroy main window created earlier

    return filename
"""


# # # Globals / Logger # # #

# # Path # #

Data = Path("data")
Logs = Data / "logs"

# # Logger # #

try:
    LogFile = Logs / f"{__name__}.log"
    LogFmt = "%(asctime)s - %(levelname)s - %(message)s"
    logger = logging.getLogger(__name__)  # Get logger with module name
    fileHndl = logging.FileHandler(LogFile)
    # Get file handler  # mode='a' OR mode='w'
    streamHndl = logging.StreamHandler()  # Get stream handler
    formatter = logging.Formatter(LogFmt)  # Get formatter

    # Handle message levels
    logger.setLevel(logging.DEBUG)
    fileHndl.setLevel(logging.DEBUG)
    streamHndl.setLevel(logging.INFO)

    # Set formatter
    fileHndl.setFormatter(formatter)
    streamHndl.setFormatter(formatter)

    # Add file handlers
    logger.addHandler(fileHndl)
    logger.addHandler(streamHndl)

except RuntimeWarning:
    # Attempt to log setup failure
    print(traceback.format_exc())
    print("An exception occured... Logging disabled.")
    # Notify setup failure outside logging
    logging.disable()  # Disable logging after failed setup


def output_directory():
    """Returns the user's desktop folder as a Path object."""
    return Path.home() / "Desktop"


class Launcher(tk.Frame):
    """A Tk application window for launching scripts."""
    def __init__(self, master: tk.Tk = None):
        """Constructor."""
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()

        self.filename = str()

    def create_widgets(self):
        """Creates and loads widgets for main launcher."""
        # Frames
        self.buttonFrm = tk.Frame(self, bd=16)  # Button frame
        self.buttonFrm.pack(side=tk.TOP)
        self.saveFrm = tk.Frame(self, bd=4)  # Save To... frame
        self.saveFrm.pack(side=tk.TOP)
        self.loadFrm = tk.Frame(self, bd=4)  # Load From... frame
        self.loadFrm.pack(side=tk.TOP)
        self.pathFrm = tk.Frame(self, bd=4)  # Option frame
        self.pathFrm.pack(side=tk.TOP)

        # Buttons and Labels #

        # Tab-Saver Button
        self.tab_saverBttn = tk.Button(self.buttonFrm, text="Save Tabs",
                                       command=(
                                           lambda: tabs.tab_saver(
                                               self.pathLbl["text"]
                                               )
                                           ))
        self.tab_saverBttn.pack(side=tk.TOP)

        # Tab-Opener Button  # IS THAT HOW YOU SPELL OPENNER?
        self.tab_openerBttn = tk.Button(self.buttonFrm, text="Open Tabs",
                                        command=(
                                            lambda: tabs.tab_opener(
                                                self.pathLbl["text"]
                                                )
                                            ))
        self.tab_openerBttn.pack(side=tk.BOTTOM)

        # Non-Button Frames
        # Change path labels and buttons
        self.pathLbl = tk.Label(self.pathFrm,
                                text=output_directory(),
                                relief=tk.SUNKEN,
                                bg="white")
        self.save_tabsLbl = tk.Label(self.saveFrm,
                                     text="Save tabs to:")
        self.choose_savefileBttn = tk.Button(self.saveFrm, text="...",
                                              command=(
                                                  lambda: self.update_path(
                                                      "Save File"
                                                      )
                                                  ))
        self.open_tabsLbl = tk.Label(self.loadFrm,
                                     text="Open tabs from:")
        self.choose_loadfileBttn = tk.Button(self.loadFrm, text="...",
                                              command=(
                                                  lambda: self.update_path(
                                                      "Open File"
                                                      )
                                                  ))
        self.save_tabsLbl.pack(side=tk.LEFT)
        self.choose_savefileBttn.pack(side=tk.LEFT)
        self.open_tabsLbl.pack(side=tk.LEFT)
        self.choose_loadfileBttn.pack(side=tk.LEFT)
        self.pathLbl.pack(side=tk.BOTTOM)

    def output_directory(self) -> str:
        """Returns the user's desktop folder as a str."""
        return self.pathLbl["text"]

    def update_path(self, Type: str) -> None:
        """Updates launcher's path using tkinter's filedialog.askdirectory,
        and updates the label to the chosen file."""
        Prompt = "Select the directory for the output files..."
        Empty = (None, "", [], (), {})

        outputDir = self.output_directory()
        if Type == "Directory":
            new_outputDir = filedialog.askdirectory(title=Prompt,
                                                    initialdir=outputDir)
        elif Type == "Open File":
            new_outputDir = filedialog.askopenfilename(title=Prompt,
                                                       initialdir=outputDir)
        elif Type == "Save File":
            # new_outputDir = filedialog.askdirectory(title=Prompt,
            #                                         initialdir=outputDir)
            # self.filename = pyautogui.prompt("Enter the filename: ")
            new_outputDir = filedialog.asksaveasfilename(title=Prompt,
                                                         initialdir=outputDir)
        else:
            raise RuntimeException

        # if cancel button is pressed...
        if new_outputDir not in Empty:
            self.pathLbl.config(text=new_outputDir)

        return None



def main():
    try:
        # Setup main window
        root = tk.Tk()
        launcher = Launcher(master=root)
        launcher.master.title("Tab-Saver Launcher")
        launcher.master.minsize(400, 200)
        launcher.master.maxsize(1000, 400)

        # Run the window
        launcher.mainloop()
    except Exception:
        # Alert user and log
        logger.critical(traceback.format_exc())
        pyautogui.alert(traceback.format_exc())

    return


if __name__ == "__main__":
    main()
